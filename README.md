# Currents.dev - GitLab CI/CD Example

This is an example repository that showcases using Gitlab CI/CD with [Currents.dev](https://currents.dev).

The example [CI config file](https://gitlab.com/currents.dev/gitlab-currents-example/-/blob/main/.gitlab-ci.yml):

- runs 3 containers with cypress tests in parallel

- installs `cypress-cloud` npm package

- Note: set your project id from [Currents.dev](https://app.currents.dev) in `currents.config.js`

- Note: use CLI arguments to customize your cypress runs, e.g.: `cypress-cloud run --parallel --record --key <your currents.dev key> --group groupA`

- Note: create a project, get your record key on [Currents.dev](https://app.currents.dev) and set `CURRENTS_RECORD_KEY` [GitLab CI/CD variable for the project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
